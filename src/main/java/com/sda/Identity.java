package com.sda;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.sql.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
public class Identity extends AbstractBaseEntity {

    private String documentId;
    private String issuedBy;
    private Date issuedDate;
    private String issuedCountry;
    private Date validUntil;
    private Integer customerId;

}
