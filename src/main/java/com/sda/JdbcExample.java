package com.sda;

import com.sda.connection.ConnectionServiceFactory;
import com.sda.connection.MysqlConnectionServiceFactory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class JdbcExample {

    public static void main(String[] args) throws SQLException {
        System.out.println("Hello Jdbc Example Fun!!!");
        ConnectionServiceFactory connectionServiceFactory = new MysqlConnectionServiceFactory("jdbc:mysql://localhost:3306/sda?serverTimezone=UTC", "root", "root");

        final ResultSet customerResultSet = connectionServiceFactory.getResult("select * from customer");

        final List<Customer> customers = new ArrayList<>();
        while (customerResultSet.next()) {
            Customer customer = new Customer();
            customer.setId(customerResultSet.getInt("id"));
            customer.setName(customerResultSet.getString("name"));
            customer.setSurname(customerResultSet.getString("surname"));
            customer.setMonthlyIncome(customerResultSet.getBigDecimal("monthlyIncome"));
            customer.setDateOfBirth(customerResultSet.getDate("dateOfBirth"));
            customer.setBankId(customerResultSet.getInt("bankId"));
            customer.setIdentityId(customerResultSet.getInt("identityId"));
            customer.setCreatedAt(customerResultSet.getDate("createdAt"));
            customer.setCreatedBy(customerResultSet.getString("createdBy"));
            customer.setUpdatedAt(customerResultSet.getDate("updatedAt"));
            customer.setUpdatedBy(customerResultSet.getString("updatedBy"));
            customers.add(customer);
        }

        for (Customer customer : customers) {
            final ResultSet identityResultSet = connectionServiceFactory.getResult("select * from identity as i where i.id=" + customer.getIdentityId());
            while (identityResultSet.next()) {
                Identity identity = new Identity();
                identity.setId(identityResultSet.getInt("id"));
                identity.setDocumentId(identityResultSet.getString("documentId"));
                identity.setIssuedBy(identityResultSet.getString("issuedBy"));
                identity.setIssuedDate(identityResultSet.getDate("issuedDate"));
                identity.setIssuedCountry(identityResultSet.getString("issuedCountry"));
                identity.setValidUntil(identityResultSet.getDate("validUntil"));
                identity.setCustomerId(identityResultSet.getInt("customerId"));

                customer.setIdentity(identity);
            }
        }

        for (Customer customer : customers) {
            final ResultSet bankResultSet = connectionServiceFactory.getResult("select * from bank as b where b.id=" + customer.getBankId());
            while (bankResultSet.next()) {
                Bank bank = new Bank();
                bank.setId(bankResultSet.getInt("id"));
                bank.setName(bankResultSet.getString("name"));
                bank.setCountry(bankResultSet.getString("country"));

                customer.setBank(bank);
            }
        }

        for (Customer customer : customers) {
            final ResultSet accountResultSet = connectionServiceFactory.getResult("select * from account as a where a.customerId=" + customer.getId());
            List<Account> accounts = new ArrayList<>();
            while (accountResultSet.next()) {
                Account account = new Account();
                account.setId(accountResultSet.getInt("id"));
                account.setAccountNumber(accountResultSet.getString("accountNumber"));
                account.setAccountName(accountResultSet.getString("accountName"));
                account.setAccountBalance(accountResultSet.getBigDecimal("accountBalance"));
                account.setCustomerId(accountResultSet.getInt("customerId"));
                final String isActive = accountResultSet.getString("isActive");
                if (isActive != null && "Y".equals(isActive)) {
                    account.setIsActive(true);
                } else {
                    account.setIsActive(false);
                }

                accounts.add(account);
            }
            customer.setAccounts(accounts);
        }

        for (Customer customer : customers) {
            for (Account account : customer.getAccounts()) {
                final ResultSet accountCurrencyResultSet = connectionServiceFactory.getResult("select * from account_currency as ac where ac.accountId=" + account.getId());
                final List<AccountCurrency> accountCurrencies = new ArrayList<>();
                while (accountCurrencyResultSet.next()) {
                    final AccountCurrency accountCurrency = new AccountCurrency();
                    accountCurrency.setId(accountCurrencyResultSet.getInt("id"));
                    accountCurrency.setAccountId(accountCurrencyResultSet.getInt("accountId"));
                    accountCurrency.setCurrencyId(accountCurrencyResultSet.getInt("currencyId"));
                    accountCurrencies.add(accountCurrency);
                }
                account.setAccountCurrencies(accountCurrencies);

                final List<Currency> currencies = new ArrayList<>();
                for (AccountCurrency accountCurrency : accountCurrencies) {
                    final ResultSet currencyResultSet = connectionServiceFactory.getResult("select * from currency as c where c.id=" + accountCurrency.getCurrencyId());
                    while (currencyResultSet.next()) {
                        final Currency currency = new Currency();
                        currency.setId(currencyResultSet.getInt("id"));
                        currency.setCode(currencyResultSet.getString("code"));
                        currency.setName(currencyResultSet.getString("name"));

                        currencies.add(currency);
                    }
                }

                account.setCurrencies(currencies);
            }
        }

        final ResultSet allInformationResultSet = connectionServiceFactory.getResult("select * from customeraccountinthebankview");
        List<CustomerAccount> customerAccounts = new ArrayList<>();
        while (allInformationResultSet.next()) {
            CustomerAccount customerAccount = new CustomerAccount();
            customerAccount.setBankName(allInformationResultSet.getString("bankName"));
            customerAccount.setCountry(allInformationResultSet.getString("country"));
            customerAccount.setCustomerName(allInformationResultSet.getString("customerName"));
            customerAccount.setCustomerSurname(allInformationResultSet.getString("customerSurname"));
            customerAccount.setMonthlyIncome(allInformationResultSet.getBigDecimal("monthlyIncome"));
            customerAccount.setAccountNumber(allInformationResultSet.getString("accountNumber"));
            customerAccount.setAccountName(allInformationResultSet.getString("accountName"));
            customerAccount.setCurrencyCode(allInformationResultSet.getString("currencyCode"));
            customerAccount.setCurrencyName(allInformationResultSet.getString("currencyName"));

            customerAccounts.add(customerAccount);
        }


        customers.stream().forEach(customer -> System.out.println(customer));
        customerAccounts.stream().forEach(customerAccount -> System.out.println(customerAccount));
    }

}
