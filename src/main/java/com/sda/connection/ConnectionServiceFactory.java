package com.sda.connection;

import java.sql.ResultSet;

public interface ConnectionServiceFactory {

    void open(final String url, final String username, final String password);

    ResultSet getResult(final String sql);

    void manipulate(final String sql);

    void close();
}
