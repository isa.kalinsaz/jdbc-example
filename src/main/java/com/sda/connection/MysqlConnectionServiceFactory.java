package com.sda.connection;

import lombok.SneakyThrows;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;


public class MysqlConnectionServiceFactory implements ConnectionServiceFactory {

    private Connection connection = null;

    public MysqlConnectionServiceFactory(final String url, final String username, final String password) {
        open(url, username, password);
    }

    @Override
    @SneakyThrows
    public void open(final String url, final String username, final String password) {
        connection = DriverManager.getConnection(url, username, password);
    }

    @Override
    @SneakyThrows
    public ResultSet getResult(final String sql) {
        final Statement sqlStatement = connection.createStatement();
        return sqlStatement.executeQuery(sql);
    }

    @Override
    @SneakyThrows
    public void manipulate(final String sql) {
        final Statement sqlStatement = connection.createStatement();
        sqlStatement.executeLargeUpdate(sql);
    }


    @Override
    @SneakyThrows
    public void close() {
        connection.close();
    }

}
