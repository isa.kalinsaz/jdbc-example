package com.sda;

import com.sda.entity.CustomerEntityServiceFactory;
import com.sda.entity.EntityServiceFactory;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

public class NewJdbcExample {

    public static void main(String[] args) {

        final EntityServiceFactory<Customer, Integer> entityServiceFactory = new CustomerEntityServiceFactory();

        System.out.println("###### Get All Customers ########");
        entityServiceFactory.getEntities().stream().forEach(customer -> System.out.println(customer));

        System.out.println("\n###### Get Customer By Id ########");
        entityServiceFactory.getEntitiesById(1).stream().forEach(customer -> System.out.println(customer));

        final Customer customer = Customer.builder().name("Dmitri").surname("Marinen").dateOfBirth(Date.valueOf(LocalDate.of(1990, 3, 21)))
                .monthlyIncome(BigDecimal.valueOf(1000)).bankId(1).build();
        //entityServiceFactory.createEntity(customer);

        // Updating existing entity
        final List<Customer> customerEntities = entityServiceFactory.getEntitiesById(1);
        if (!customerEntities.isEmpty()) {
            Customer customerEntity = customerEntities.get(0);
            customerEntity.setDateOfBirth(Date.valueOf(LocalDate.of(1990, 1, 1)));
            customerEntity.setMonthlyIncome(BigDecimal.valueOf(1500));
            entityServiceFactory.updateEntity(customerEntity);
        }

        // deleting existing entity
        final List<Customer> customerEntityForDelete = entityServiceFactory.getEntitiesById(6);
        if (!customerEntityForDelete.isEmpty()) {
            entityServiceFactory.deleteById(customerEntityForDelete.get(0).getId());
        }
    }
}
