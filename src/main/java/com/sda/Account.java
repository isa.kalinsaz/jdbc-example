package com.sda;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = true)
public class Account extends AbstractBaseEntity {

    private String accountNumber;
    private String accountName;
    private BigDecimal accountBalance;
    private Boolean isActive;
    private Integer customerId;

    private List<AccountCurrency> accountCurrencies;
    private List<Currency> currencies;

}
