package com.sda;

import lombok.Data;

import java.io.Serializable;
import java.sql.Date;

@Data
public abstract class AbstractBaseEntity implements Serializable {

    private Integer id;
    private Date createdAt;
    private String createdBy;
    private Date updatedAt;
    private String updatedBy;

}
