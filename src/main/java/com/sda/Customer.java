package com.sda;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
public class Customer extends AbstractBaseEntity {

    private String name;
    private String surname;
    private Date dateOfBirth;
    private BigDecimal monthlyIncome;
    private Integer bankId;
    private Integer identityId;

    private Identity identity;
    private Bank bank;
    private List<Account> accounts;

}
