package com.sda;

import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;

@Data
@ToString(callSuper = true)
public class CustomerAccount extends AbstractBaseEntity {

    private String bankName;
    private String country;
    private String customerName;
    private String customerSurname;
    private BigDecimal monthlyIncome;
    private String accountNumber;
    private String accountName;
    private String currencyCode;
    private String currencyName;

}
