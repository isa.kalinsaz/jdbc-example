package com.sda;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AccountCurrency extends AbstractBaseEntity {

    private Integer accountId;
    private Integer currencyId;

}
