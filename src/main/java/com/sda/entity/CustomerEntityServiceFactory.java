package com.sda.entity;

import com.sda.Customer;
import com.sda.connection.ConnectionServiceFactory;
import com.sda.connection.MysqlConnectionServiceFactory;
import lombok.SneakyThrows;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CustomerEntityServiceFactory implements EntityServiceFactory<Customer, Integer> {

    final ConnectionServiceFactory connectionServiceFactory = new MysqlConnectionServiceFactory("jdbc:mysql://localhost:3306/sda?serverTimezone=UTC", "root", "root");

    @Override
    public void createEntity(final Customer newEntity) {
        final StringBuilder sqlBuilder = new StringBuilder();
        sqlBuilder.append("INSERT INTO customer(name,surname,dateOfBirth,monthlyIncome,bankId,identityId) ");
        sqlBuilder.append("VALUES('" + newEntity.getName() + "', '" + newEntity.getSurname() + "', '" + newEntity.getDateOfBirth() + "', " + newEntity.getMonthlyIncome() + ", " + newEntity.getBankId() + ", " + newEntity.getIdentityId() + " )");

        connectionServiceFactory.manipulate(sqlBuilder.toString());
    }

    @Override
    public void updateEntity(final Customer existingEntity) {
        final StringBuilder sqlBuilder = new StringBuilder();
        sqlBuilder.append("UPDATE customer as c ");
        sqlBuilder.append("SET c.name='" + existingEntity.getName() + "',c.surname='" + existingEntity.getSurname() + "',c.dateOfBirth='" + existingEntity.getDateOfBirth() + "'" +
                ",c.monthlyIncome=" + existingEntity.getMonthlyIncome() + ",c.bankId=" + existingEntity.getBankId() + ",c.identityId=" + existingEntity.getIdentityId() + " ");
        sqlBuilder.append("WHERE c.id=" + existingEntity.getId());

        connectionServiceFactory.manipulate(sqlBuilder.toString());
    }

    @Override
    public void deleteById(final Integer entityId) {
        final StringBuilder sqlBuilder = new StringBuilder();
        sqlBuilder.append("DELETE FROM customer c WHERE c.id=" + entityId);

        connectionServiceFactory.manipulate(sqlBuilder.toString());
    }

    @Override
    @SneakyThrows
    public List<Customer> getEntitiesById(final Integer customerId) {
        final ResultSet customerResultSet = connectionServiceFactory.getResult("select * from customer as c where c.id=" + customerId);
        return getCustomers(customerResultSet);
    }

    @Override
    @SneakyThrows
    public List<Customer> getEntities() {
        final ResultSet customerResultSet = connectionServiceFactory.getResult("select * from customer");
        return getCustomers(customerResultSet);
    }

    private List<Customer> getCustomers(final ResultSet customerResultSet) throws SQLException {
        final List<Customer> customers = new ArrayList<>();
        while (customerResultSet.next()) {
            Customer customer = new Customer();
            customer.setId(customerResultSet.getInt("id"));
            customer.setIdentityId(customerResultSet.getInt("identityId"));
            customer.setCreatedAt(customerResultSet.getDate("createdAt"));
            customer.setCreatedBy(customerResultSet.getString("createdBy"));
            customer.setUpdatedAt(customerResultSet.getDate("updatedAt"));
            customer.setUpdatedBy(customerResultSet.getString("updatedBy"));
            customer.setName(customerResultSet.getString("name"));
            customer.setSurname(customerResultSet.getString("surname"));
            customer.setMonthlyIncome(customerResultSet.getBigDecimal("monthlyIncome"));
            customer.setDateOfBirth(customerResultSet.getDate("dateOfBirth"));
            customer.setBankId(customerResultSet.getInt("bankId"));
            customers.add(customer);
        }
        return customers;
    }
}
