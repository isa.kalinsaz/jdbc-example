package com.sda.entity;

import com.sda.AbstractBaseEntity;

import java.io.Serializable;
import java.util.List;

public interface EntityServiceFactory<CLASS extends AbstractBaseEntity, TYPE extends Serializable> {

    void createEntity(final CLASS newEntity);

    void updateEntity(final CLASS existingEntity);

    void deleteById(final TYPE entityId);

    List<CLASS> getEntitiesById(TYPE referenceEntity);

    List<CLASS> getEntities();
}
