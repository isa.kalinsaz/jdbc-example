package com.sda.entity;

import com.sda.Account;
import com.sda.connection.ConnectionServiceFactory;
import com.sda.connection.MysqlConnectionServiceFactory;
import lombok.SneakyThrows;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AccountEntityServiceFactory implements EntityServiceFactory<Account, Integer> {

    final ConnectionServiceFactory connectionServiceFactory = new MysqlConnectionServiceFactory("jdbc:mysql://localhost:3306/sda?serverTimezone=UTC", "root", "root");

    @Override
    public void createEntity(final Account newEntity) {
        final StringBuilder sqlBuilder = new StringBuilder();
        final String isActive = newEntity.getIsActive() ? "Y" : "N";
        sqlBuilder.append("INSERT INTO account(accountNumber,accountName,accountBalance,isActive,customerId) ");
        sqlBuilder.append("VALUES('" + newEntity.getAccountNumber() + "', '" + newEntity.getAccountName() + "', " + newEntity.getAccountBalance() + ", '" + isActive + "', " + newEntity.getCustomerId() + " )");

        connectionServiceFactory.manipulate(sqlBuilder.toString());
    }

    @Override
    public void updateEntity(final Account existingEntity) {

    }

    @Override
    public void deleteById(final Integer entityId) {

    }


    @Override
    @SneakyThrows
    public List<Account> getEntitiesById(final Integer customerId) {
        final ResultSet accountResultSet = connectionServiceFactory.getResult("select * from account as a where a.customerId=" + customerId);
        return getAccounts(accountResultSet);
    }

    @Override
    @SneakyThrows
    public List<Account> getEntities() {
        final ResultSet accountResultSet = connectionServiceFactory.getResult("select * from account as a");
        return getAccounts(accountResultSet);
    }

    private List<Account> getAccounts(final ResultSet accountResultSet) throws SQLException {
        final List<Account> accounts = new ArrayList<>();
        while (accountResultSet.next()) {
            Account account = new Account();
            account.setId(accountResultSet.getInt("id"));
            account.setCreatedAt(accountResultSet.getDate("createdAt"));
            account.setCreatedBy(accountResultSet.getString("createdBy"));
            account.setUpdatedAt(accountResultSet.getDate("updatedAt"));
            account.setUpdatedBy(accountResultSet.getString("updatedBy"));
            account.setAccountNumber(accountResultSet.getString("accountNumber"));
            account.setAccountName(accountResultSet.getString("accountName"));
            account.setAccountBalance(accountResultSet.getBigDecimal("accountBalance"));
            account.setCustomerId(accountResultSet.getInt("customerId"));
            final String isActive = accountResultSet.getString("isActive");
            if (isActive != null && "Y".equals(isActive)) {
                account.setIsActive(true);
            } else {
                account.setIsActive(false);
            }
            accounts.add(account);
        }
        return accounts;
    }

}
